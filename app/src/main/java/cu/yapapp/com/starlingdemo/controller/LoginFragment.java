package cu.yapapp.com.starlingdemo.controller;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import cu.yapapp.com.starlingdemo.R;
import cu.yapapp.com.starlingdemo.model.controllerServiceManager.LoginManager;
import cu.yapapp.com.starlingdemo.utils.UserInfo;
import cu.yapapp.com.starlingdemo.view.views.LoginView;
import pay.eu.android.Utils;
import pay.eu.android.http.pg.WebController;
import pay.eu.android.http.pg.WebControllerHelper;
import pay.eu.android.model.AccessTokenModel;

public class LoginFragment extends YABaseFragment {

    public LoginFragment() {
        // Required empty public constructor
    }

    private LoginView loginView;
    private LoginManager loginManager;
    private WebController webController;
    private cu.yapapp.com.starlingdemo.utils.Utils utils;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        utils = cu.yapapp.com.starlingdemo.utils.Utils.getInstance();

        webController = new WebController(getActivity());

        loginManager = new LoginManager(  webController, loginManagerCallback);

        loginView = new LoginView(getContext(), loginManager.getLoginCallback());


        if (!utils.isAuthTokenPresent(getContext())) {
//            show login screen
            loginView.loadUrl(Utils.getOauthWebUrl());
        } else {
            popSingleFragment();
//             show DashboardScreen
            replaceFragment(new PaymentFragment(), true, true);
        }

        return loginView;
    }


    @Override
    public void onResume() {
        super.onResume();
        ((YABaseActivity) getActivity()).showHideAppBar(false);
    }

    private LoginManager.LoginManagerCallback loginManagerCallback = new LoginManager.LoginManagerCallback() {

        @Override
        public void openIntent(Intent intent) {
            startActivity(intent);
        }

        @Override
        public void onAccessTokenReceived(AccessTokenModel accessTokenModel, String body) {

            UserInfo.getInstance().setAccessToken(accessTokenModel);

            WebControllerHelper.saveAccessTokenPref(getActivity(), accessTokenModel.getAccessToken());
            Toast.makeText(getActivity(), getString(R.string.login_successful), Toast.LENGTH_LONG).show();
            accessTokenModel.setTokenFetchedTimeMilli(Long.toString(System.currentTimeMillis()  ));
            cu.yapapp.com.starlingdemo.utils.
                    Utils.getInstance()
                    .saveAccessTokenModel(LoginFragment.this.getContext(), accessTokenModel);

            popSingleFragment();
            replaceFragment(new PaymentFragment(), true, true);

        }

        @Override
        public void onErrorWhileLogin(String body) {
            /**
             * No code for now as Starling needs to be updated.
             */
        }


    };

}
