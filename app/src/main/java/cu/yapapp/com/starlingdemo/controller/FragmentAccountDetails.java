package cu.yapapp.com.starlingdemo.controller;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cu.yapapp.com.starlingdemo.R;
import cu.yapapp.com.starlingdemo.model.controllerServiceManager.AccountDetailsManager;
import cu.yapapp.com.starlingdemo.utils.StarlingWebController;
import cu.yapapp.com.starlingdemo.utils.UserInfo;
import cu.yapapp.com.starlingdemo.view.views.AccountDetailsView;
import pay.eu.android.model.AccountModel;
import pay.eu.android.model.BalanceModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentAccountDetails extends FragmentAppbar   {

    private AccountDetailsView accountDetailsView;
    private AccountDetailsManager accountDetailsManager;


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        /**
         * Check is accountDetailsView is null then initialise variables otherwise return accountDetailsView .
         */
        if (accountDetailsView != null) return accountDetailsView;

        accountDetailsView = new AccountDetailsView(getContext());

        accountDetailsManager = new AccountDetailsManager(new StarlingWebController(getActivity()), managerInterface);

        accountDetailsManager.requestAccountDetails();


        return accountDetailsView;
    }

    @Override
    public void onResume() {
        super.onResume();

        setTitleTxt(getString(R.string.bank_account));
    }

    AccountDetailsManager.AccountDetailsManagerInterface managerInterface = new AccountDetailsManager.AccountDetailsManagerInterface() {
        @Override
        public void onAccountDetailReceived(AccountModel object) {

            /**
             * When account details are received two operations are performed.
             * 1) Save them to Utility for further use
             * 2) Show account information in UI.
             * */

            UserInfo.getInstance().setAccountModel(object);

            accountDetailsView.showAccountDetails(object);
        }

        @Override
        public void onBalanceDetailReceived(BalanceModel object) {


            /**
             * When show details on UI after fetching.
             * */
            accountDetailsView.showBalance(object);
        }

        @Override
        public void onError(String body) {
            accountDetailsView.showMessage(getString(R.string.error_server_connection));
        }

    };



}
