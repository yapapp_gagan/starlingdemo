package cu.yapapp.com.starlingdemo.view.adapters;

import android.content.Context;
import android.support.v7.graphics.drawable.DrawerArrowDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import cu.yapapp.com.starlingdemo.R;
import cu.yapapp.com.starlingdemo.utils.Utils;
import pay.eu.android.model.ContactModel;
import pay.eu.android.model.TransactionHistoryModel;
import pay.eu.android.model.TransactionModel;

/**
 * Created by gaganpreet on 12/9/17.
 */

public class TransactionsAdapter extends RecyclerView.Adapter<TransactionsAdapter.PayeesHolder> {


    private List<TransactionModel> models;
    private Context context;


    public TransactionsAdapter(Context context, List<TransactionModel> transactions) {
        this.context = context;
        this.models = transactions;
    }


    @Override
    public void onBindViewHolder(PayeesHolder holder, final int position) {

        TransactionModel transactionModel = models.get(position);
        Utils.getInstance().getCircularResourceImage(context, R.drawable.placeholder, holder.ivPayee);
        try {
            Date date = Utils.getInstance().parseTime("2017-09-12T04:54:35.906Z");
            String dateParsed = Utils.getInstance().parseDate(date);
            holder.tvTime.setText(dateParsed);
        } catch (ParseException e) {
            e.printStackTrace();
            holder.tvTime.setText("");
        }

        double amount = transactionModel.getAmount();

        if (amount < 0) {
            amount = -amount;
        }

        holder.tvTransactionStatusInfo.setText(Utils.getInstance().convertToTitleCase(transactionModel.getDirection()));
        holder.tvAmount.setText(Utils.getInstance().parseDouble(amount));
        holder.tvTransactionStatus.setText(transactionModel.getSource());
        holder.tvAccInfo.setText(transactionModel.getNarrative());
        Utils.getInstance().getCircularResourceImage(context, R.drawable.placeholder, holder.ivPayee);
    }

    @Override
    public PayeesHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_transaction, viewGroup, false);
        PayeesHolder mViewHolder = new PayeesHolder(view);
        return mViewHolder;
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    class PayeesHolder extends RecyclerView.ViewHolder {

        private TextView tvAccInfo;
        private ImageView ivPayee;
        private TextView tvTime;
        private TextView tvAmount;
        private TextView tvTransactionStatus;
        private TextView tvTransactionStatusInfo;

        public PayeesHolder(View itemView) {
            super(itemView);

            tvAccInfo = (TextView) itemView.findViewById(R.id.tvAccInfo);
            tvTime = (TextView) itemView.findViewById(R.id.tvTime);
            tvAmount = (TextView) itemView.findViewById(R.id.tvAmount);
            tvTransactionStatus = (TextView) itemView.findViewById(R.id.tvTransactionStatus);
            tvTransactionStatusInfo = (TextView) itemView.findViewById(R.id.tvTransactionStatusInfo);

            ivPayee = (ImageView) itemView.findViewById(R.id.ivPayee);


        }
    }


}
