package cu.yapapp.com.starlingdemo.controller;

import android.content.DialogInterface;
import android.os.Bundle;

import cu.yapapp.com.starlingdemo.R;
import cu.yapapp.com.starlingdemo.utils.Utils;

public class MainActivity extends YABaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        showHideSearch(false);

        replaceFragment(new SplashFragment(), true, true);
    }


    @Override
    public void onBackPressed() {

        if (getCurrentFragment() instanceof PaymentFragment) {
            Utils.getInstance().showDialog(this, getString(R.string.exit_message), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
        } else {
            super.onBackPressed();
        }
    }
}
