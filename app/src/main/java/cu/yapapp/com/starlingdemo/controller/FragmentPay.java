package cu.yapapp.com.starlingdemo.controller;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import cu.yapapp.com.starlingdemo.R;
import cu.yapapp.com.starlingdemo.model.controllerServiceManager.PayManager;
import cu.yapapp.com.starlingdemo.utils.StarlingWebController;
import cu.yapapp.com.starlingdemo.view.views.PayView;
import pay.eu.android.model.ContactModel;

/**
 * Created by gaganpreet on 14/9/17.
 */

public class FragmentPay extends FragmentAppbar {

    private PayView payView;
    private ContactModel contact;
    private PayManager payManager;


    /**
     * @param contact Contact of benificiary for Payment.
     */
    public void setContact(ContactModel contact) {
        this.contact = contact;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (payView != null) return payView;

        payView = new PayView(getContext(), payViewListener);

        payManager = new PayManager(new StarlingWebController(getActivity()), callbacks);

        payView.setContactInfo(contact);
        return payView;
    }


    @Override
    public void onResume() {
        super.onResume();

        setTitleTxt(getString(R.string.pay));
    }


    PayManager.PayeesServiceManagerCallbacks callbacks = new PayManager.PayeesServiceManagerCallbacks() {
        @Override
        public void onError(String body) {
            payView.showMessage(getString(R.string.error_server_connection));
        }

        @Override
        public void onPaymentSuccessful(String body) {
            payView.showMessage(getString(R.string.payment_successful));
            popSingleFragment();
        }
    };


    PayView.PayViewListener payViewListener = new PayView.PayViewListener() {
        @Override
        public void onPayClicked(String amount, String comment) {
            /**
             * Initiation payment when user click on Pay button.
             */
            payManager.payToContact(contact, amount, comment);
        }
    };

}
