package cu.yapapp.com.starlingdemo.utils;

import android.app.Activity;
import android.base.http.WebHandler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import pay.eu.android.http.pg.WebController;
import pay.eu.android.model.AccessTokenModel;
import retrofit2.Response;

/**
 * Created by gaganpreet on 12/9/17.
 */

public class StarlingWebController extends WebController {

    Utils utils = Utils.getInstance();

    public StarlingWebController(Activity activity) {
        super(activity);
    }


    @Override
    public void getBalanceWebCall(final WebHandler.OnWebCallback responseCallback, final boolean isDialog) {
        boolean isAccessTokenValid = utils.isAuthTokenValid(getActivity());
        if (!isAccessTokenValid) {
            refreshToken(new RefreshCallbackImpl(responseCallback) {
                @Override
                public void onAccessTokenUpdatedSuccessfully() {
                    getBalanceWebCall(responseCallback, isDialog);
                }
            });
        } else
            super.getBalanceWebCall(responseCallback, isDialog);
    }


    @Override
    public void getAccountsWebCall(final WebHandler.OnWebCallback responseCallback, @NonNull final String accessToken, final boolean isDialog) {


        boolean isAccessTokenValid = utils.isAuthTokenValid(getActivity());
        if (!isAccessTokenValid) {
            refreshToken(new RefreshCallbackImpl(responseCallback) {
                @Override
                public void onAccessTokenUpdatedSuccessfully() {
                    getAccountsWebCall(responseCallback, accessToken, isDialog);
                }
            });
        } else
            super.getAccountsWebCall(responseCallback, accessToken, isDialog);

    }

    @Override
    public void getAccountsWebCall(final WebHandler.OnWebCallback responseCallback, final boolean isDialog) {

        boolean isAccessTokenValid = utils.isAuthTokenValid(getActivity());
        if (!isAccessTokenValid) {
            refreshToken(new RefreshCallbackImpl(responseCallback) {
                @Override
                public void onAccessTokenUpdatedSuccessfully() {
                    getAccountsWebCall(responseCallback,  isDialog);
                }
            });
        } else
            super.getAccountsWebCall(responseCallback, isDialog);

    }

    @Override
    public void getPayeesWebCall(final WebHandler.OnWebCallback responseCallback, @NonNull final String accessToken, final boolean isDialog) {
        boolean isAccessTokenValid = utils.isAuthTokenValid(getActivity());
        if (!isAccessTokenValid) {
            refreshToken(new RefreshCallbackImpl(responseCallback) {
                @Override
                public void onAccessTokenUpdatedSuccessfully() {
                    getPayeesWebCall(responseCallback, accessToken, isDialog);
                }
            });
        } else
            super.getPayeesWebCall(responseCallback, accessToken, isDialog);
    }


    @Override
    public void getPayeesWebCall(final WebHandler.OnWebCallback responseCallback, final boolean isDialog) {
        boolean isAccessTokenValid = utils.isAuthTokenValid(getActivity());
        if (!isAccessTokenValid) {
            refreshToken(new RefreshCallbackImpl(responseCallback) {
                @Override
                public void onAccessTokenUpdatedSuccessfully() {
                    getPayeesWebCall(responseCallback, isDialog);
                }
            });
        } else
            super.getPayeesWebCall(responseCallback, isDialog);
    }


    @Override
    public void getPaymentHistoryWebCall(final WebHandler.OnWebCallback responseCallback, @NonNull final String accessToken, final boolean isDialog) {

        boolean isAccessTokenValid = utils.isAuthTokenValid(getActivity());
        if (!isAccessTokenValid) {
            refreshToken(new RefreshCallbackImpl(responseCallback) {
                @Override
                public void onAccessTokenUpdatedSuccessfully() {
                    getPaymentHistoryWebCall(responseCallback, accessToken, isDialog);
                }
            });
        } else
            super.getPaymentHistoryWebCall(responseCallback, accessToken, isDialog);

    }


    @Override
    public void getPaymentHistoryWebCall(final WebHandler.OnWebCallback responseCallback, final boolean isDialog) {

        boolean isAccessTokenValid = utils.isAuthTokenValid(getActivity());
        if (!isAccessTokenValid) {
            refreshToken(new RefreshCallbackImpl(responseCallback) {
                @Override
                public void onAccessTokenUpdatedSuccessfully() {
                    getPaymentHistoryWebCall(responseCallback, isDialog);
                }
            });
        } else
            super.getPaymentHistoryWebCall(responseCallback, isDialog);

    }

    private void refreshToken(final RefreshCallback refreshCallback) {
        String refreshToken = utils.getAccessTokenModel(getActivity()).getRefreshToken();
        refreshAccessToken(refreshToken, new WebHandler.OnWebCallback() {
                    @Override
                    public <T> void onSuccess(@Nullable T t, int i, Response response) {
                        refreshCallback.onSuccess(t, i, response);
                        AccessTokenModel accessTokenModel = (AccessTokenModel) t;
                        accessTokenModel.setTokenFetchedTimeMilli(Long.toString(System.currentTimeMillis() ));
                        utils.saveAccessTokenModel(getActivity(), accessTokenModel);
                        refreshCallback.onAccessTokenUpdatedSuccessfully();
                    }

                    @Override
                    public <T> void onError(@Nullable T t, String s, int i, Response response) {
                        refreshCallback.onError(t, s, i, response);
                    }
                }
                , true
        );
    }


    /**
     * Class for handling Refresh callbacks. Incase error persists we send callback to the calling class of {@link StarlingWebController}.
     */
    private abstract class RefreshCallbackImpl implements RefreshCallback {
        private WebHandler.OnWebCallback onWebCallback;

        public RefreshCallbackImpl(WebHandler.OnWebCallback onWebCallback) {
            this.onWebCallback = onWebCallback;
        }

        @Override
        public <T> void onSuccess(@Nullable T t, int i, Response response) {
        }

        @Override
        public <T> void onError(@Nullable T t, String s, int i, Response response) {
            onWebCallback.onError(t, s, i, response);
        }
    }


    /***
     * This interface holds properties of {@link android.base.http.WebHandler.OnWebCallback} also has one more property to send callback when Access token updated.
     */
    private interface RefreshCallback extends WebHandler.OnWebCallback {
        public void onAccessTokenUpdatedSuccessfully();
    }

}

