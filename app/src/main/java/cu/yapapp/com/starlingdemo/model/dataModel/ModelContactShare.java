package cu.yapapp.com.starlingdemo.model.dataModel;

/**
 * Created by gaganpreet on 13/9/17.
 */

public class ModelContactShare {

    private String id;
    private String sortCode;
    private String account;
    private String name;

    public ModelContactShare(String id, String accountNumber, String sortCode, String name) {
        this.id = id;
        this.account = accountNumber;
        this.sortCode = sortCode;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getSortCode() {
        return sortCode;
    }

    public String getAccount() {
        return account;
    }

    public String getName() {
        return name;
    }
}
