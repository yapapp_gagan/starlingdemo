package cu.yapapp.com.starlingdemo.controller;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cu.yapapp.com.starlingdemo.R;
import cu.yapapp.com.starlingdemo.model.controllerServiceManager.MyAccountInformationManager;
import cu.yapapp.com.starlingdemo.utils.StarlingWebController;
import cu.yapapp.com.starlingdemo.utils.UserInfo;
import cu.yapapp.com.starlingdemo.utils.Utils;
import cu.yapapp.com.starlingdemo.view.views.MyBarcodeView;
import pay.eu.android.model.AccountModel;

/**
 * Created by gaganpreet on 13/9/17.
 */

public class MyBarcodeFragment extends FragmentAppbar {


    private MyBarcodeView myBarcodeView;
    private MyAccountInformationManager myAccountInformationManager;
    private StarlingWebController starlingWebController;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (myBarcodeView != null) return myBarcodeView;


        myBarcodeView = new MyBarcodeView(getContext());

        starlingWebController = new StarlingWebController(getActivity());

        myAccountInformationManager = new MyAccountInformationManager(starlingWebController, myAccountInformationManagerListener);

        AccountModel accountModel = UserInfo.getInstance().getAccountModel();


        if (accountModel!=null){
            fillInformation(accountModel);
        }else{
            myAccountInformationManager.requestAccountDetail();
        }


        return myBarcodeView;
    }


    @Override
    public void onResume() {
        super.onResume();
        setTitleTxt(getString(R.string.my_profile));
    }

    /**
     * This shows barcode by fetching details from {@link AccountModel}
     * @param accountModel
     */
    private void fillInformation(AccountModel accountModel) {

        if (accountModel != null) {
            String data = Utils.getInstance().getContactJson(accountModel);
            myBarcodeView.setText(data);
        }

    }

    MyAccountInformationManager.MyAccountInformationManagerListener myAccountInformationManagerListener = new MyAccountInformationManager.MyAccountInformationManagerListener() {
        @Override
        public void onAccountDetailsReceived(AccountModel accountModel) {
            fillInformation(accountModel);
        }

        @Override
        public void onError(String data) {
            /**
             * No code required for now as Starling sdk needs to be updated.
             */
        }
    };


}
