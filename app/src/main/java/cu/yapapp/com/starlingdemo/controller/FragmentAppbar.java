package cu.yapapp.com.starlingdemo.controller;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;

import cu.yapapp.com.starlingdemo.R;

/**
 * Created by gaganpreet on 15/9/17.
 */

public class FragmentAppbar extends YABaseFragment {

    private YABaseActivity yaBaseActivity;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getActivity() instanceof YABaseActivity) {
            yaBaseActivity = (YABaseActivity) getActivity();
        }

    }

    @Override
    public void onResume() {
        super.onResume();

        /**
         * Show Appbar and left button
         */
        if (yaBaseActivity != null) {
            yaBaseActivity.showHideAppBar(true);
            showBackButtonOnLeft();
        }
    }


    /**
     * Clear left and right buttons from {@link android.support.v7.widget.Toolbar}
     */
    public void removeLeftRight() {
        removeRightIconsView();
        removeLeftIconsView();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        if (yaBaseActivity != null) {
            yaBaseActivity.showHideAppBar(false);
        }
    }


    /**
     * It adds Back button to the fragments toolbar.
     */
    private void showBackButtonOnLeft() {
        removeRightIconsView();
        removeLeftIconsView();
        setSearchIconVisibility(false);
        ImageView ivBack = new ImageView(getActivity());
        ivBack.setPadding((int) getResources().getDimension(R.dimen.left_back_icon_padding), 0, 0, 0);
        ivBack.setImageResource(R.drawable.ic_arrow_back_black_24dp);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popSingleFragment();
            }
        });
        addLeftIcon(ivBack);
    }


}
