package cu.yapapp.com.starlingdemo.view.views;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import cu.yapapp.com.starlingdemo.R;
import cu.yapapp.com.starlingdemo.utils.Utils;

/**
 * Created by gaganpreet on 14/9/17.
 */

public class PaymentView extends SuperView {

    private RelativeLayout rlMyProfile;
    private RelativeLayout rlBankAccount;
    private RelativeLayout rlBenificiaries;
    private RelativeLayout rlTransactions;
    private RelativeLayout rlLogout;
    private PaymentViewCallbacks paymentViewCallbacks;


    public PaymentView(Context context, PaymentViewCallbacks paymentViewCallbacks) {
        super(context);
        this.paymentViewCallbacks = paymentViewCallbacks;
        initValiables();
        setEventCallbacks();
    }

    @Override
    public void initValiables() {
        inflate(getContext(), R.layout.fragment_payment, this);


        rlMyProfile = (RelativeLayout) findViewById(R.id.rlMyProfile);
        fillInformationInChilds(rlMyProfile, getContext().getString(R.string.my_profile), R.drawable.ic_exit_to_app_black_24dp);


        rlBankAccount = (RelativeLayout) findViewById(R.id.rlBankAccount);
        fillInformationInChilds(rlBankAccount, getContext().getString(R.string.bank_account), R.drawable.ic_exit_to_app_black_24dp);


        rlBenificiaries = (RelativeLayout) findViewById(R.id.rlBenificiaries);
        fillInformationInChilds(rlBenificiaries, getContext().getString(R.string.beneficiaries), R.drawable.ic_exit_to_app_black_24dp);


        rlTransactions = (RelativeLayout) findViewById(R.id.rlTransactions);
        fillInformationInChilds(rlTransactions, getContext().getString(R.string.transactions), R.drawable.ic_exit_to_app_black_24dp);


        rlLogout = (RelativeLayout) findViewById(R.id.rlLogout);
        fillInformationInChilds(rlLogout, getContext().getString(R.string.logout), R.drawable.ic_exit_to_app_black_24dp);


    }

    private void fillInformationInChilds(View parent, String string, int res) {

        TextView tvTitle = (TextView) parent.findViewById(R.id.tvTitle);
        tvTitle.setText(string);

        ImageView ivMyInfo = (ImageView) parent.findViewById(R.id.ivMyInfo);
        ivMyInfo.setImageResource(res);

    }

    @Override
    public void setEventCallbacks() {
        rlMyProfile.setOnClickListener(optionsClick);
        rlBankAccount.setOnClickListener(optionsClick);
        rlBenificiaries.setOnClickListener(optionsClick);
        rlTransactions.setOnClickListener(optionsClick);
        rlLogout.setOnClickListener(optionsClick);
    }


    OnClickListener optionsClick = new OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.rlMyProfile:
                    paymentViewCallbacks.onMyProfileClick();
                    break;

                case R.id.rlBankAccount:
                    paymentViewCallbacks.onBankAccountClick();
                    break;


                case R.id.rlBenificiaries:
                    paymentViewCallbacks.onBeneficiariesClick();
                    break;

                case R.id.rlTransactions:
                    paymentViewCallbacks.onTransactionsClick();
                    break;

                case R.id.rlLogout:

                    Utils.getInstance().showDialog(getContext(), getResources().getString(R.string.logout_message), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            paymentViewCallbacks.onLogoutClick();
                        }
                    });

                    break;
                default:
//                     no code for default
                    break;
            }
        }
    };


    public interface PaymentViewCallbacks {
        /**
         * Callback when user click My Profile
         */
        public void onMyProfileClick();

        /**
         * Callback when user click Bank Account
         */
        public void onBankAccountClick();


        /**
         * Callback when user click Benificiaries
         */
        public void onBeneficiariesClick();



        /**
         * Callback when user click Transactions
         */
        public void onTransactionsClick();


        /**
         * Callback when user click Logout
         */
        public void onLogoutClick();
    }


}
