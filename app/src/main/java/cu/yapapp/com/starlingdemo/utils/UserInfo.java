package cu.yapapp.com.starlingdemo.utils;

import pay.eu.android.model.AccessTokenModel;
import pay.eu.android.model.AccountModel;
import pay.eu.android.model.PayeeModel;

/**
 * Created by gaganpreet on 13/9/17.
 */

public class UserInfo {

    private static UserInfo userInfo = new UserInfo();
    private AccessTokenModel accessToken;
    private AccountModel accountModel;
    private PayeeModel payees;
    private BarcodeDetectedListener barcodeDetectedListener;


    private UserInfo() {
    }


    /**
     *
     * @return instance of {@link UserInfo}
     */
    public static UserInfo getInstance() {
        return userInfo;
    }


    /**
     * Setter for accessToken
     * @param accessToken
     */
    public void setAccessToken(AccessTokenModel accessToken) {
        this.accessToken = accessToken;
    }

    /**
     * It returns {@link AccountModel} instance
     * @return
     */
    public AccountModel getAccountModel() {
        return accountModel;
    }


    /**
     *  It stores {@link AccountModel} instance inside the class.
     * @param accountModel
     */
    public void setAccountModel(AccountModel accountModel) {
        this.accountModel = accountModel;
    }

    /**
     * It Saves PayeesModel which can be used in app.
     * @param payees
     */
    public void setPayees(PayeeModel payees) {
        this.payees = payees;
    }


    /**
     * Setter for barcodeDetectedListener
     * @param barcodeDetectedListener
     */
    public void setBarcodeDetectedListener(BarcodeDetectedListener barcodeDetectedListener) {
        this.barcodeDetectedListener = barcodeDetectedListener;
    }

    /**
     * Getter for barcodeDetectedListener instance.
     * @return
     */
    public BarcodeDetectedListener getBarcodeDetectedListener() {
        return barcodeDetectedListener;
    }


    public interface BarcodeDetectedListener {
        /**
         *  Callback comes when barcode is detected
         * @param data data of user to request for adding.
         */
        public void onBarcodeDetected(String data);
    }


    /**
     * Getter for accessToken
     * @return
     */
    public AccessTokenModel getAccessToken() {
        return accessToken;
    }

    /**
     * Getter for payees
     * @return
     */
    public PayeeModel getPayees() {
        return payees;
    }


}
