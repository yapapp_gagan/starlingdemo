package cu.yapapp.com.starlingdemo.view.views;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import cu.yapapp.com.starlingdemo.R;
import cu.yapapp.com.starlingdemo.view.adapters.TransactionsAdapter;
import pay.eu.android.model.TransactionHistoryModel;

/**
 * Created by gaganpreet on 15/9/17.
 */

public class TransactionsView extends SuperView {

    private RecyclerView rvTransations ;
    private TransactionsAdapter transactionsAdapter ;

    public TransactionsView(Context context) {
        super(context);
        initValiables();
        setEventCallbacks();
    }

    @Override
    public void initValiables() {
        inflate(getContext(), R.layout.fragment_transactions, this);
        rvTransations = (RecyclerView) findViewById(R.id.rlTransactions);
        rvTransations.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    @Override
    public void setEventCallbacks() {
//        no view for capturing events
    }

    public void loadTransactionHistory(TransactionHistoryModel payeeModel) {
        transactionsAdapter = new TransactionsAdapter(  getContext() , payeeModel.getEmbeddedData().getTransactions());
        rvTransations.setAdapter(transactionsAdapter);
    }
}
