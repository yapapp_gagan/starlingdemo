package cu.yapapp.com.starlingdemo.model.controllerServiceManager;

import android.app.Activity;
import android.base.http.WebHandler;
import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;

import cu.yapapp.com.starlingdemo.utils.StarlingWebController;
import cu.yapapp.com.starlingdemo.utils.UserInfo;
import pay.eu.android.model.AccountModel;
import pay.eu.android.model.BalanceModel;
import retrofit2.Response;

/**
 * Created by gaganpreet on 12/9/17.
 */

public class AccountDetailsManager {

    private StarlingWebController webController;
    private AccountDetailsManagerInterface managerInterface;


    public AccountDetailsManager(StarlingWebController starlingWebController, AccountDetailsManagerInterface managerInterface) {
        this.webController = starlingWebController;
        this.managerInterface = managerInterface;
    }


    /**
     * This method check if we have account details in {@link UserInfo} class or need to be
     * fetch from the server.
     */
    public void requestAccountDetails() {
        AccountModel accountModel = UserInfo.getInstance().getAccountModel();

        if (accountModel == null)
            webController.getAccountsWebCall(getAccountsWebCall, true);
        else
            taskOnAccountsReceived(accountModel);
    }


    /**
     * Callback to handle Balance Api result.
     */
    WebHandler.OnWebCallback getBalanceWebCall = new WebHandler.OnWebCallback() {
        @Override
        public <T> void onSuccess(@Nullable T t, int i, Response response) {
            BalanceModel balanceModel = (BalanceModel) t;
            managerInterface.onBalanceDetailReceived(balanceModel);
        }

        @Override
        public <T> void onError(@Nullable T t, String s, int i, Response response) {
            managerInterface.onError((String) response.body());
        }
    };



    /**
     * Callback to handle Account Api callback.
     */
    WebHandler.OnWebCallback getAccountsWebCall = new WebHandler.OnWebCallback() {
        @Override
        public <T> void onSuccess(@Nullable T t, int i, Response response) {
            AccountModel accountModel = (AccountModel) t;

            taskOnAccountsReceived(accountModel);
        }

        @Override
        public <T> void onError(@Nullable T t, String s, int i, Response response) {
            managerInterface.onError((String) response.body());
        }
    };

    /**
     * In this method we pass account details to the controller and request Balance details from Starling.
     * @param accountModel Account model
     */
    private void taskOnAccountsReceived(AccountModel accountModel) {
        managerInterface.onAccountDetailReceived(accountModel);
        requestBalanceDetails();
    }

    /**
     * This method need to be called to fetch Balance details.
     */
    public void requestBalanceDetails() {
        webController.getBalanceWebCall(getBalanceWebCall, true);
    }

    public interface AccountDetailsManagerInterface {

        /**
         *
         * This method pass {@link AccountModel} instance to the controller.
         *
         * @param object {@link AccountModel} instance
         */
        public void onAccountDetailReceived(AccountModel object);

        /**
         * * This method pass {@link BalanceModel} instance to the controller.
         * @param object {@link BalanceModel} instance.
         */
        public void onBalanceDetailReceived(BalanceModel object);


        /**
         * We pass description of error to the controller.
         *
         * @param body String
         */
        public void onError(String body);

    }

}
