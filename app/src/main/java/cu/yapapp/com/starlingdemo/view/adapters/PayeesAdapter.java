package cu.yapapp.com.starlingdemo.view.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import cu.yapapp.com.starlingdemo.R;
import cu.yapapp.com.starlingdemo.utils.Utils;
import pay.eu.android.model.ContactModel;

/**
 * Created by gaganpreet on 12/9/17.
 */

public class PayeesAdapter extends RecyclerView.Adapter<PayeesAdapter.PayeesHolder> {


    private List<ContactModel> models;
    private PayeeAdapterCallback payeeAdapterCallback;
    private Context context;


    public PayeesAdapter(Context context, List<ContactModel> models, PayeeAdapterCallback payeeAdapterCallback) {
        this.context = context;
        this.models = models;
        this.payeeAdapterCallback = payeeAdapterCallback;
    }


    public interface PayeeAdapterCallback {

        /**
         *  It is called when user click on a payee
         * @param contactModel
         */
        public void open(ContactModel contactModel);

    }

    @Override
    public void onBindViewHolder(PayeesHolder holder, final int position) {

        ContactModel contactModel = models.get(position);

        holder.tvName.setText(contactModel.getName());

        holder.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                payeeAdapterCallback.open(models.get(position));
            }
        });


        Utils.getInstance().getCircularResourceImage(context, R.drawable.placeholder, holder.ivPayee);
    }

    @Override
    public PayeesHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_payee, viewGroup, false);
        PayeesHolder mViewHolder = new PayeesHolder(view);
        return mViewHolder;
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    class PayeesHolder extends RecyclerView.ViewHolder {

        private TextView tvName;
        private View root;
        private ImageView ivPayee;

        public PayeesHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tvPayeeName);
            ivPayee = (ImageView) itemView.findViewById(R.id.ivPayee);
            root = itemView.findViewById(R.id.root);
        }
    }


}
