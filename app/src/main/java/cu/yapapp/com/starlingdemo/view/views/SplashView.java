package cu.yapapp.com.starlingdemo.view.views;

import android.content.Context;

import cu.yapapp.com.starlingdemo.R;
import cu.yapapp.com.starlingdemo.model.controllerServiceManager.SplashManager;

/**
 * Created by gaganpreet on 11/9/17.
 */

public class SplashView extends SuperView  {

    public SplashView(Context context) {
        super(context);
        initValiables();
    }

    @Override
    public void initValiables() {

        inflate(getContext()  , R.layout.content_splash , this);

    }

    @Override
    public void setEventCallbacks() {
//          no view for capturing events
    }

}
