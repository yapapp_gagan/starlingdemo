package cu.yapapp.com.starlingdemo.view.views;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.base.util.ApplicationUtils;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import cu.yapapp.com.starlingdemo.R;

/**
 * Created by gaganpreet on 11/9/17.
 */

public class LoginView extends SuperView {

    private LoginViewCallbacks loginViewCallbacks;
    private WebView webView;
    private ProgressDialog progressDialog;

    public LoginView(Context context, LoginViewCallbacks loginViewCallbacks) {
        super(context);
        this.loginViewCallbacks = loginViewCallbacks;
        initValiables();
        setEventCallbacks();
    }

    @Override
    public void initValiables() {
        inflate(getContext(), R.layout.fragment_login, this);
        webView = (WebView) findViewById(R.id.webview);

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please wait while we are loading.");
        progressDialog.setCancelable(false);

        config(webView);
    }


    /**
     *  This method opens login url in webview
     * @param webUrl : url to load in webview
     */
    public void loadUrl(String webUrl) {
        webView.loadUrl(webUrl);
    }


    /**
     *  This method is used to configure settings of webview.
     * @param webView
     */
    private void config(WebView webView) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) {
            webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        }
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setGeolocationEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
    }

    @Override
    public void setEventCallbacks() {
        webView.setWebViewClient(new WebViewClient());
        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onJsAlert(WebView view, String url,
                                     String message, JsResult result) {
                ApplicationUtils.Log.d(getClass().getSimpleName(), "onJsAlert = " + url);
                return true;
            }
        });


        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                // stop progress mDialog
                progressDialog.dismiss();
                super.onPageFinished(view, url);
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                // start progress mDialog
                progressDialog.show();
                super.onPageStarted(view, url, favicon);
            }

            @SuppressWarnings("deprecation")
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                final Uri uri = Uri.parse(url);
                return loginViewCallbacks.handleOverrideUrl(uri);
            }


            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                final Uri uri = request.getUrl();
                return loginViewCallbacks.handleOverrideUrl(uri);
            }

        });


    }


    public interface LoginViewCallbacks {
        /**
         * When webview loads any url it calls this method.
         * @param url
         * @return
         */
        public boolean handleOverrideUrl(Uri url);
    }


}
