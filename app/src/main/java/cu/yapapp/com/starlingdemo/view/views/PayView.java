package cu.yapapp.com.starlingdemo.view.views;

import android.content.Context;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import cu.yapapp.com.starlingdemo.R;
import cu.yapapp.com.starlingdemo.utils.Utils;
import pay.eu.android.model.ContactModel;

/**
 * Created by gaganpreet on 14/9/17.
 */

public class PayView extends SuperView {

    private TextView tvName;

    private EditText etAmount;
    private EditText etComment;
    private TextView tvPay;
    private ImageView ivPayee;

    private PayViewListener payViewCallbacks;

    public PayView(Context context, PayViewListener payViewCallbacks) {
        super(context);
        this.payViewCallbacks = payViewCallbacks;
        initValiables();
        setEventCallbacks();
    }

    @Override
    public void initValiables() {
        inflate(getContext(), R.layout.fragment_pay, this);
        tvName = (TextView) findViewById(R.id.tvPayeeName);

        etAmount = (EditText) findViewById(R.id.etAmount);
        etComment = (EditText) findViewById(R.id.etComment);
        tvPay = (TextView) findViewById(R.id.tvPay);
        ivPayee = (ImageView) findViewById(R.id.ivPayee);


        Utils.getInstance().getCircularResourceImage(getContext(), R.drawable.placeholder, ivPayee);

    }

    @Override
    public void setEventCallbacks() {
        tvPay.setOnClickListener(onPayClick);
    }

    public void setContactInfo(ContactModel contactModel) {
        tvName.setText(contactModel.getName());
    }


    OnClickListener onPayClick = new OnClickListener() {
        @Override
        public void onClick(View v) {
            String comment = etComment.getText().toString();
            String amount = etAmount.getText().toString();
            payViewCallbacks.onPayClicked(amount, comment);

        }
    };


    public interface PayViewListener {
        public void onPayClicked(String amount, String comment);
    }

}
