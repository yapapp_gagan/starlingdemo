package cu.yapapp.com.starlingdemo.controller;

import android.app.Activity;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;

/**
 * Created by yapapp on 24/11/16.
 */
public class YABaseFragment extends Fragment {

    boolean isOnCreateViewCalled = false;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);


    }

    public boolean toPop = false;


    @Override
    public void onResume() {
        super.onResume();

        ((YABaseActivity) getActivity()).showHideAppBar(showAppBar());
        ((YABaseActivity) getActivity()).enableDisableDrawer(showDrawer());
       // removeLeftIconsView();
      //  removeRightIconsView();
        if (toPop) {
            popSingleFragment();
        }
    }

    public void replaceFragment(YABaseFragment fragment, boolean add) {
        try {
            ((YABaseActivity) getActivity()).replaceFragment(fragment, add);
            logError("relpacingFragment", fragment.getClass().getCanonicalName() + ":" + add);

        } catch (Exception e) {
            e.printStackTrace();
            logError("relpacingFragment", "Exception e" + add);
        }
    }

    public void addFragment(YABaseFragment fragment, boolean add) {
        try {
            ((YABaseActivity) getActivity()).addFragment(fragment, add);
            logError("addFragment", fragment.getClass().getCanonicalName() + ":" + add);

        } catch (Exception e) {
            e.printStackTrace();
            logError("addFragment", "Exception e" + add);
        }
    }


    public void replaceFragment(YABaseFragment fragment, String tag, boolean add, boolean showAnimation) {
        try {
            ((YABaseActivity) getActivity()).replaceFragment(fragment, tag, add, showAnimation);
            logError("relpacingFragment", fragment.getClass().getCanonicalName() + ":" + add);

        } catch (Exception e) {
            e.printStackTrace();
            logError("relpacingFragment", "Exception e" + add);
        }
    }


    public void replaceFragment(YABaseFragment fragment, boolean add, boolean showAnimation) {
        try {
            ((YABaseActivity) getActivity()).replaceFragment(fragment, add, showAnimation);
            logError("relpacingFragment", fragment.getClass().getCanonicalName() + ":" + add);

        } catch (Exception e) {
            e.printStackTrace();
            logError("relpacingFragment", "Exception e" + add);
        }


    }

    public void addFragment(YABaseFragment fragment, boolean add, boolean showAnimation) {
        try {
            ((YABaseActivity) getActivity()).addFragment(fragment, add, showAnimation);
            logError("addFragment", fragment.getClass().getCanonicalName() + ":" + add);

        } catch (Exception e) {
            e.printStackTrace();
            logError("addFragment", "Exception e" + add);
        }
    }

    public void showSnackAlert(String message) {
        ((YABaseActivity) getActivity()).showAlert(message);
    }

    public boolean showAppBar() {
        return false;
    }


    public TabLayout getTabs() {
        return  /* ((YABaseActivity)getActivity()).tablayout*/new TabLayout(getContext());
    }

    public void setTitle(String title) {
        ((YABaseActivity) getActivity()).getToolBar().setTitle(title);
    }

    public boolean showDrawer() {
        return false;
    }

    public void popSingleFragment() {
        try {
            ((YABaseActivity) getActivity()).popSingle();
            toPop = false;
        } catch (IllegalStateException e) {
            toPop = true;
            throw e;
        }
    }


    public void logError(String tag, String error) {
        Log.e(tag, error);
    }

   /* public void showRightIcon(boolean bool) {

        if (bool) {
            ((YABaseActivity) getActivity()).imgBtnRight.setVisibility(View.VISIBLE);
            ((YABaseActivity) getActivity()).relRightIcon.setVisibility(View.VISIBLE);
        } else {
            ((YABaseActivity) getActivity()).imgBtnRight.setVisibility(View.INVISIBLE);
            ((YABaseActivity) getActivity()).relRightIcon.setVisibility(View.INVISIBLE);
        }

    }*/

  /*  public void showLeftIcon(boolean bool) {

        if (bool) {
            ((YABaseActivity) getActivity()).imgBtnLeft.setVisibility(View.VISIBLE);
        } else {
            ((YABaseActivity) getActivity()).imgBtnLeft.setVisibility(View.INVISIBLE);
        }

    }*/


    /**
     * Add icon in the Container
     *
     * @param view
     */
    public void addRightIcon(View view) {
        ((YABaseActivity) getActivity()).llRightContainer.addView(view);
    }


    /**
     * Add icon in the Container
     *
     * @param view
     */
    public void addLeftIcon(View view) {
        ((YABaseActivity) getActivity()).llLeftContainer.addView(view);
    }

    /**
     * Remove the views added in the right container
     */
    public void removeRightIconsView() {
        ((YABaseActivity) getActivity()).llRightContainer.removeAllViews();
    }


    /**
     * Hide show Visibilty of right container
     *
     * @param visibility
     */
    public void hideRightContainer(boolean visibility) {
        if(((YABaseActivity) getActivity()).llRightContainer==null){
            return;
        }
        if (visibility) {
            ((YABaseActivity) getActivity()).llRightContainer.setVisibility(View.VISIBLE);
        } else {
            ((YABaseActivity) getActivity()).llRightContainer.setVisibility(View.GONE);
        }
    }


    /**
     * Set the visibilty of the serach view
     *
     * @param visibility
     */
    public void setSearchIconVisibility(boolean visibility) {
        if(((YABaseActivity) getActivity()).mImg_search==null){
            return;
        }
        if (visibility) {
            ((YABaseActivity) getActivity()).mImg_search.setVisibility(View.VISIBLE);
        } else {
            ((YABaseActivity) getActivity()).mImg_search.setVisibility(View.GONE);
        }

       /* if(((YABaseActivity) getActivity()).svSearchHolder==null){
            return;
        }
        if (visibility) {
            ((YABaseActivity) getActivity()).svSearchHolder.setVisibility(View.VISIBLE);
        } else {
            ((YABaseActivity) getActivity()).svSearchHolder.setVisibility(View.GONE);
        }*/
    }








    /**
     * Remove the views added in the left container
     */
    public void removeLeftIconsView() {
        ((YABaseActivity) getActivity()).llLeftContainer.removeAllViews();
    }




  /*  public void setRightIcon(int resourceFile) {

        ((YABaseActivity) getActivity()).imgBtnRight.setImageResource(resourceFile);
    }

    public void setLeftIcon(int resourceFile) {
        ((YABaseActivity) getActivity()).imgBtnLeft.setImageResource(resourceFile);
    }


    public ImageView getLeftIconView() {
        return ((YABaseActivity) getActivity()).imgBtnLeft;
    }


    public ImageView getRightIconView() {
        return ((YABaseActivity) getActivity()).imgBtnRight;
    }*/

    public android.widget.SearchView getSearchView() {
        try {
            return ((YABaseActivity) getActivity()).getSearchView();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public void setTitleTxt(String titleTxt) {
        ((YABaseActivity) getActivity()).title.setText(titleTxt);
    }

    public void setSubTitleTxt(String titleTxt) {
        ((YABaseActivity) getActivity()).tvSubTitle.setVisibility(View.VISIBLE);
        ((YABaseActivity) getActivity()).tvSubTitle.setText(titleTxt);
    }

    /**
     * Set the subtitle text visibility to gone
     */
    public void hideSubTitleText(){
        ((YABaseActivity) getActivity()).tvSubTitle.setVisibility(View.GONE);
    }

  /*  public YAView getView() {
        return  null;
    }
*/
  public int get_SearchViewTag()
  {
      if(((YABaseActivity) getActivity()).mImg_search==null)
      {
          return 0;
      }

      else
      {
          return Integer.parseInt((((YABaseActivity) getActivity()).mImg_search).getTag().toString());
      }
  }

    public void closeSearchBar()
    {

        ((YABaseActivity) getActivity()).closeSearchView();
    }


    public void setSearchListener(YABaseActivity.SearchMenuInterface searchMenuInterface) {
        ((YABaseActivity) getActivity()).initSearchView(searchMenuInterface);
    }


}
