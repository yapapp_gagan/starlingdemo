package cu.yapapp.com.starlingdemo.model.controllerServiceManager;

import android.base.http.WebHandler;
import android.support.annotation.Nullable;

import cu.yapapp.com.starlingdemo.utils.StarlingWebController;
import pay.eu.android.model.AccountModel;
import retrofit2.Response;

/**
 * Created by gaganpreet on 14/9/17.
 */

public class MyAccountInformationManager {


    private MyAccountInformationManagerListener myAccountInformationManagerListener;
    private StarlingWebController webController;

    public MyAccountInformationManager(StarlingWebController webController, MyAccountInformationManagerListener myAccountInformationManagerListener) {
        this.webController = webController;
        this.myAccountInformationManagerListener = myAccountInformationManagerListener;
    }


    public interface MyAccountInformationManagerListener {

        /**
         * Sends {@link AccountModel} to controller
         * @param accountModel
         */
        public void onAccountDetailsReceived(AccountModel accountModel);

        /**
         * Sends error to controller
         * @param data
         */
        public void onError(String data);
    }


    public void requestAccountDetail() {
        webController.getAccountsWebCall(getAccountsWebCall, true);
    }


    WebHandler.OnWebCallback getAccountsWebCall = new WebHandler.OnWebCallback() {
        @Override
        public <T> void onSuccess(@Nullable T t, int i, Response response) {
            AccountModel accountModel = (AccountModel) t;
            myAccountInformationManagerListener.onAccountDetailsReceived(accountModel);
        }

        @Override
        public <T> void onError(@Nullable T t, String s, int i, Response response) {
            myAccountInformationManagerListener.onError((String) response.body());
        }
    };


}
