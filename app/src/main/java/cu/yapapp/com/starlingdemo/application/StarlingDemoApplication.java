package cu.yapapp.com.starlingdemo.application;

import android.app.Application;

import cu.yapapp.com.starlingdemo.R;
import pay.eu.android.Config;

/**
 * Created by gaganpreet on 11/9/17.
 */

public class StarlingDemoApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        initialiseStartling();
    }

    /**
     * This method is used for initialising mendatory fields in starling SDK.
     * Removal of any can leads to unwanted errors.
     */
    private void initialiseStartling() {
        Config.setClientId(getString(R.string.starling_client_id));
        Config.setClientSecret(getString(R.string.starling_client_secret));
        Config.setBaseWebUrlOauthCallback(getString(R.string.callback_url));
        android.base.http.WebConstant.setBaseUrl("https://api-sandbox.starlingbank.com/");
        android.base.http.WebConstant.setApiVersion("api/v1/");
    }
}
