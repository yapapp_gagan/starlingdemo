package cu.yapapp.com.starlingdemo.model.controllerServiceManager;

import android.base.http.WebHandler;
import android.support.annotation.Nullable;

import java.util.LinkedHashMap;
import java.util.Map;

import cu.yapapp.com.starlingdemo.utils.StarlingWebController;
import pay.eu.android.http.pg.WebConstant;
import pay.eu.android.model.ContactModel;
import pay.eu.android.model.PayeeDetailModel;
import pay.eu.android.model.PayeeDetailResponseModel;
import pay.eu.android.model.TransactionModel;
import retrofit2.Response;

/**
 * Created by gaganpreet on 14/9/17.
 */

public class PayManager {

    private StarlingWebController mStarlingWebController;
    private PayeesServiceManagerCallbacks payeesServiceManagerCallbacks;


    public PayManager(StarlingWebController mStarlingWebController, PayeesServiceManagerCallbacks payeesServiceManagerCallbacks) {
        this.mStarlingWebController = mStarlingWebController;
        this.payeesServiceManagerCallbacks = payeesServiceManagerCallbacks;
    }


    /**
     * @param contactModel Benificiary
     * @param amount       Amount to send
     * @param comment      Any comment.
     */
    public void payToContact(ContactModel contactModel, final String amount, final String comment) {

        String temAccountId = contactModel.getId();

        getPayeeDetail(temAccountId, new PayeeDetail() {
            @Override
            public void onPayeeReturned(PayeeDetailResponseModel payeeDetailResponseModel) {
//                     perform payment
                pay(payeeDetailResponseModel, amount, comment);
            }
        });

    }


    /**
     *
     * This methos is used to get PayeeDetail before initiating payment to get {@link PayeeDetailResponseModel} for AccountNumber .
     *
     * @param userId User id of benificiary
     * @param payeeDetail detail of payee
     */
    private void getPayeeDetail(String userId, final PayeeDetail payeeDetail) {
        mStarlingWebController.getPayeeDetailWebCall(userId, new WebHandler.OnWebCallback() {
            @Override
            public <T> void onSuccess(@Nullable T t, int i, Response response) {
                payeeDetail.onPayeeReturned((PayeeDetailResponseModel) t);
            }

            @Override
            public <T> void onError(@Nullable T t, String s, int i, Response response) {
                payeesServiceManagerCallbacks.onError((String) response.body());
            }
        }, true);
    }


    /**
     * This method is used to transfer money from user to a benificiary.
     * @param payeeDetailResponseModel
     * @param amount amount to send
     * @param comment narrative
     */
    private void pay(PayeeDetailResponseModel payeeDetailResponseModel, String amount, String comment) {
        PayeeDetailModel payeeDetailModel = payeeDetailResponseModel.getAccounts().get(0);
        Map<String, Object> params = new LinkedHashMap<>();
        params.put(WebConstant.DESTINATION_ACCOUNT_ID_PARAM, payeeDetailModel.getId());
        /**
         * Dont remove the commented code below.
         */
//        params.put(WebConstant.SORT_CODE_PARAM, payeeDetailModel.getSortCode());
        TransactionModel transactionModel = new TransactionModel();
        transactionModel.setAmount(Double.parseDouble(amount));
        transactionModel.setCurrency("GBP");
        /**
         * Dont remove the commented code below.
         */
//        transactionModel.setNarrative(comment);
//        ToDo: adding payment narrative i.e. message is not added in starling sdk yet.
        params.put(WebConstant.PAYMENT_PARAM, transactionModel);
        params.put(WebConstant.PAYMENT_COMMENT_REFERENCE_PARAM, comment);
        mStarlingWebController.payLocalEuWebCall(params, new WebHandler.OnWebCallback() {
            @Override
            public <T> void onSuccess(@Nullable T t, int i, Response response) {
                payeesServiceManagerCallbacks.onPaymentSuccessful((String) response.body());
            }

            @Override
            public <T> void onError(@Nullable T t, String s, int i, Response response) {
                payeesServiceManagerCallbacks.onError((String) response.body());
            }
        }, true);
    }


    /**
     * Interface to send callbacks to the controller
     */
    public interface PayeesServiceManagerCallbacks {

        /**
         *  This is used to callback of error to the controller when the payment was unsuccessful.
         * @param body
         */
        public void onError(String body);

        /**
         *
         * @param body String body when payment is successful
         */
        public void onPaymentSuccessful(String body);

    }

    private interface PayeeDetail {
        /**
         * method called when payee is returned.
         * @param payeeDetailResponseModel intance that holds payee information like account number and sortcode.
         */
        public void onPayeeReturned(PayeeDetailResponseModel payeeDetailResponseModel);
    }


}
