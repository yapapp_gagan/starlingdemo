package cu.yapapp.com.starlingdemo.model.controllerServiceManager;

import android.os.Handler;

/**
 * Created by gaganpreet on 11/9/17.
 */

public class SplashManager {

    private SplashManagerCallback splashManagerCallback;
    private final int TIME = 2000;
    private Handler handler =new Handler();

    public SplashManager(SplashManagerCallback splashManagerCallback) {
        this.splashManagerCallback = splashManagerCallback;
    }

    public void startCounting() {
        handler.postDelayed(runnable, TIME);
    }


    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            splashManagerCallback.openDashboard();
        }
    };


    public interface SplashManagerCallback {
        /**
         * Callback to to openDashboard when splash time is over.
         */
        public void openDashboard();
    }

}
